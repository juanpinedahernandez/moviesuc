package com.udacity.movies.Utils;

/**
 * Created by Laura Pineda on 07/08/2017.
 */

public class Constants {

    public static final String API_KEY ="";
    public static final String URL_POPULAR ="https://api.themoviedb.org/3/movie/popular?api_key="+API_KEY;
    public static final String URL_RATED ="https://api.themoviedb.org/3/movie/top_rated?api_key="+API_KEY;

}
